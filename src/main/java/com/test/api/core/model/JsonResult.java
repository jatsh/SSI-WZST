package com.test.api.core.model;

public class JsonResult {

	private boolean isSuccess;

	private Object msg;
	//private boolean isError;

	public boolean isSuccess() {
		return isSuccess;
	}

	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}

	public Object getMsg() {
		return msg;
	}

	public void setMsg(Object msg) {
		this.msg = msg;
	}


}
