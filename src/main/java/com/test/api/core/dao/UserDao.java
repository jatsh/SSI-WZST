package com.test.api.core.dao;

import com.test.api.common.dao.*;
import com.test.api.core.model.*;
import org.springframework.stereotype.*;

import java.util.*;

@Repository("userDao")
public class UserDao extends BaseDao{

	public boolean login(User user){

		List<User> list = this.getSqlMapClientTemplate().queryForList(
				"getUser", user.getUsername());
		if (list.size() > 0
				&& user.getUserpwd().equals(list.get(0).getUserpwd())){
			return true;
		}
		return false;
	}

}
