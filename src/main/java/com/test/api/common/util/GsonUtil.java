package com.test.api.common.util;

import com.google.gson.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class GsonUtil{

	private static final Gson gson = new Gson();

	public static Gson getGson(){

		return gson;
	}

	public static String toJson(Object obj){

		return gson.toJson(obj);
	}

	public static String toJson(Collection<?> collection){

		return gson.toJson(collection);
	}

	public static <T> T toBean(String json, Class<T> classOfT){

		return gson.fromJson(json, classOfT);
	}

	public static List<?> getListFromJson(String json, Class<?> listType){

		return (ArrayList<?>) gson.fromJson(json, listType);
	}

}
