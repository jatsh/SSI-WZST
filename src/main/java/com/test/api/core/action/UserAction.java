package com.test.api.core.action;

import com.test.api.common.action.*;
import com.test.api.common.util.*;
import com.test.api.core.model.*;
import com.test.api.core.service.*;
import org.apache.struts2.*;
import org.springframework.stereotype.*;

import javax.servlet.http.*;
import java.io.*;

@Controller
public class UserAction extends BaseAction{

	private UserService userService;
	private User user;

	public User getUser(){

		return user;
	}

	public void setUser(User user){

		this.user = user;
	}

	public UserService getUserService(){

		return userService;
	}

	public void setUserService(UserService userService){

		this.userService = userService;
	}

	public void login() throws IOException{

		JsonResult jsonResult = new JsonResult();
		try{
			if (user.getUsername() == null || user.getUsername().equals("")
					|| user.getUserpwd() == null || user.getUserpwd().equals("")){
				jsonResult.setSuccess(false);
				jsonResult.setMsg("Login failed");

			} else{
				if (userService.login(user)){
					jsonResult.setSuccess(true);
					jsonResult.setMsg("Login Success");
				}
			}

		} catch (Exception e){
			jsonResult.setSuccess(false);
			jsonResult.setMsg("Login failed");
		}

		HttpServletResponse response = ServletActionContext.getResponse();
		PrintWriter         out      = response.getWriter();
		System.out.println(GsonUtil.toJson(jsonResult));
		if (out != null){

			out.write(GsonUtil.toJson(jsonResult));
			out.flush();
			out.close();
		}

	}

}
