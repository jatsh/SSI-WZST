package com.test.api.common.dao;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public abstract class BaseDao extends SqlMapClientDaoSupport {
	
	protected Object getObject(String id, Object paramObject){
		
		Object result = null;
		try {
			result = getSqlMapClientTemplate().queryForObject(id, paramObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
}
