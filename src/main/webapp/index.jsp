<%@taglib uri="/struts-tags" prefix="s"%>

<%@page contentType="text/html;charset=UTF-8" language="java"
	import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script src="scripts/jquery.min.js" type="text/javascript"></script>
<script src="scripts/jquery-ui-min.js" type="text/javascript"></script>
<script type="text/javascript">
	function getNotice() {
		alert("getNotice");
		$.ajax({
			url : 'notice!getLatestNot',
			type : 'post',
			success : function(data) {
				var json = eval("(" + data + ")");
				$("#notice .id").html(json.id);
				$("#notice .title").html(json.title);
				$("#notice .editor").html(json.editor);
				$("#notice .content").html(json.content);
				$("#notice .date").html(json.date);

			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert(XMLHttpRequest.status);
				alert(XMLHttpRequest.readyState);
				alert(errorThrown);
			}
		})

	}

	function getAllNotice() {

		$.ajax({
			url : 'notice!getAllNotice',
			type : 'post',
			success : function(data) {
				var json = eval("(" + data + ")");
				for ( var i = 0; i < json.length; i++) {
					var content = "id:<span>" + json[i].id + "    title:"
							+ json[i].title + "   content:" + json[i].content
							+ "</span></br>";
					$("#allNotice").append(content);

				}
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert(XMLHttpRequest.status);
				alert(XMLHttpRequest.readyState);
				alert(errorThrown);
			}
		})

	}

	function serchCluByType() {
		var cluType = $("#club .cluType").val();
		$.ajax({
			url : 'club!getClubsByType',
			type : 'post',
			data : {
				'type' : cluType,
			},
			success : function(data) {
				var json = eval("(" + data + ")");
				if (json.length == 0) {
					alert("无该类型社团");
					$("#club .clubInfo").html("");
				} else {
					$("#club .clubInfo").html(
							"<span>个数：" + json.length + "</span><br>");
					for ( var i = 0; i < json.length; i++) {
						var content = "id:<span>" + json[i].id + "    title:"
								+ json[i].name + "</span></br>";
						$("#club .clubInfo").append(content);

					}
				}

			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert(XMLHttpRequest.status);
				alert(XMLHttpRequest.readyState);
				alert(errorThrown);
			}
		})
	}

	function serchCluById() {
		var cluId = $("#club .cluId").val();
		$.ajax({
			url : 'club!getClubDetailById',
			type : 'post',
			data : {
				'clubId' : cluId,
			},
			success : function(data) {
				var json = eval("(" + data + ")");
				if (json.id < 0) {
					alert("社团不存在");
					$("#club .cluId").val("");
				} else {
					$("#club .clubDetail").html(data);
				}
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert(XMLHttpRequest.status);
				alert(XMLHttpRequest.readyState);
				alert(errorThrown);
			}
		})

	}

	function serchCluByName() {
		var clubName = $("#club .cluName").val();
		$.ajax({
			url : 'club!getClubDetailByName',
			type : 'post',
			data : {
				'clubName' : clubName,
			},
			success : function(data) {
				var json = eval("(" + data + ")");
				if (json.length == 0) {
					alert("无任何结果");
					$("#club .clubDetail").html("");
					$("#club .cluName").val("");
				} else {
					$("#club .clubDetail").html("");
					for ( var i = 0; i < json.length; i++) {
						var content = "id:<span>" + json[i].id + "    name:"
								+ json[i].name + "    dep:" + json[i].dep
								+ "    type:" + json[i].type
								+ "    proproeter:" + json[i].proproeter
								+ "</span></br>";
						$("#club .clubDetail").append(content);

					}
				}

			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert(XMLHttpRequest.status);
				alert(XMLHttpRequest.readyState);
				alert(errorThrown);
			}
		})

	}

	function getClubMember() {
		var clubId = $("#clubManage .clubId").val();
		$.ajax({
			url : 'club!getClubMember',
			type : 'post',
			data : {
				'clubId' : clubId,
			},
			success : function(data) {
				$("#clubManage .clubMember").html(data);

			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert(XMLHttpRequest.status);
				alert(XMLHttpRequest.readyState);
				alert(errorThrown);
			}
		})
	}
	function getUser() {
		var userId = $("#getUser .userId").val();
		$.ajax({
			url : 'user!getUserById',
			type : 'post',
			data : {
				'userId' : userId,
			},
			success : function(data) {
				$("#getUser .userInfo").html(data);
				var json = eval("(" + data + ")");
				$("#myuserId").val(json.id);
				$("#name").val(json.name);
				$("#age").val(json.age);
				$("#phone").val(json.phone);
				$("#qq").val(json.qq);
				$("#t_class").val(json.t_class);
				$("#sex").val(json.sex);
				$("#dep").val(json.dep);
				$("#bz").val(json.bz);
				
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert(XMLHttpRequest.status);
				alert(XMLHttpRequest.readyState);
				alert(errorThrown);
			}
		})

	}
	

	function updateUserInfo(){
		var id =$("#myuserId").val();
		var username =$("#username").val();
		var userpwd = $("#userpwd").val()
		var name = $("#name").val();
		var age =$("#age").val();
		var sex =$("#sex").val();
		var phone = $("#phone").val();
		var qq = $("#qq").val();
		var t_class = $("#t_class").val();
		var dep =$("#dep").val();
		var bz =$("#bz").val();
		
		$.ajax({
			url : 'user!updateUserInfo',
			type : 'post',
			data : {
				'userId' : id,
				'name' : name,
				'age' : age,
				'sex' : sex,
				'phone' : phone,
				'qq' : qq,
				't_class' : t_class,
				'dep' : dep,
				'bz' : bz,
				
			},
			success : function(data) {
				alert(data);
			//	var json = eval("(" + data + ")");
				
				
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert(XMLHttpRequest.status);
				alert(XMLHttpRequest.readyState);
				alert(errorThrown);
			}
		})
		
		
		
		
		
		
	}
</script>

<title>Insert title here</title>
</head>
<body>
	<h2>接口测试页面</h2>
	========================== 用户登录 =============================
	<s:form action="user!login" method="post">
		<s:textfield name="name" label="user.username"></s:textfield>
		<s:textfield name="password" label="user.userpwd"></s:textfield>
		<s:submit></s:submit>
	</s:form>

	=========================== 用户注册 =============================
	<s:form action="user!register" method="post">
		<s:textfield name="name" label="username"></s:textfield>
		<s:textfield name="password" label="password"></s:textfield>
		<s:submit></s:submit>
	</s:form>

	=========================== 通知公告 =============================
	</br>
	<input type="Button" onclick="getNotice()" value="最新通知">
	<div id="notice">
		<span class="id">id</span><br> <span class="title">title</span><br>
		<span class="editor">editor</span><br> <span class="content">content</span><br>
		<span class="date">date</span><br>
	</div>
	---------------------------所有通知-------------------------------
	</br>
	<input type="Button" onclick="getAllNotice()" value="所有通知">
	<div id="allNotice"></div>
	<br> =========================== 社团信息=============================
	<br>
	<div id="club">
		0:社会科学类&nbsp&nbsp 1:志愿服务类&nbsp&nbsp 2:文学艺术类&nbsp&nbsp
		3:体育健身类&nbsp&nbsp<br> 根据社团类型编号查询:<input type="text"
			name="cluType" class="cluType">
		<button onclick="serchCluByType()">查询</button>
		<br>
		<div class="clubInfo"></div>
		<br> 根据社团编号查询：<input type="text" name="cluId" class="cluId">
		<button onclick="serchCluById()">查询</button>
		<br> 根据社团名称模糊查询：<input type="text" name="cluName" class="cluName">
		<button onclick="serchCluByName()">查询</button>
		<br>
		<div class="clubDetail"></div>
	</div>
	<br> ===========================社团管理=============================
	<div id="clubManage">
		输入社团id: <input type="text" name="clubId" class="clubId">
		<button onclick="getClubMember()">查询</button>
		<div class="clubMember"></div>
	</div>
	<br>
	===========================社团人员管理=============================
	<div id="getUser">
		输入社团id: <input type="text" name="userId" class="userId">
		<button onclick="getUser()">查询</button>
		<div class="userInfo"></div>
		<div class="userInfo2">
			id：<input type="text" id="myuserId" name="myuserId" class="myuserId" size="20"><br> 
			name:<input type="text" id = "name" name="name" class="name" size="20"><br>
			age:<input type="text"  id = "age" name="age" class="age" size="20"><br>
			sex:<input type="text" id = "sex" name="sex" class="sex" size="20"><br>
			phone:<input type="text" id = "phone" name="phone" class="phonoe" size="20"><br>
			qq:<input type="text"  id ="qq" name="qq" class="qq" size="20"><br>
			class:<input type="text"  id ="t_class" name="t_class" class="t_class" size="20"><br>
			dep:<input type="text" id = "dep" name="dep" class="dep" size="20"><br>
			bz:<input type="text" id = "bz" name="bz" class="bz" size="20"><br>
			<button onclick = "updateUserInfo()">更新</button><br><br><br><br>
		</div>
	</div>

</body>
</html>