package com.test.api.core.model;

public class User{

	private int    id;
	private String username;
	private String userpwd;
	private String name;
	private int    age;
	private String sex;
	private String phone;
	private String qq;
	private String t_class;
	private String dep;
	private String bz;

	public String getUsername(){

		return username;
	}

	public void setUsername(String username){

		this.username = username;
	}

	public String getUserpwd(){

		return userpwd;
	}

	public void setUserpwd(String userpwd){

		this.userpwd = userpwd;
	}

	public int getAge(){

		return age;
	}

	public void setAge(int age){

		this.age = age;
	}

	public String getPhone(){

		return phone;
	}

	public void setPhone(String phone){

		this.phone = phone;
	}

	public String getQq(){

		return qq;
	}

	public void setQq(String qq){

		this.qq = qq;
	}

	public String getT_class(){

		return t_class;
	}

	public void setT_class(String t_class){

		this.t_class = t_class;
	}

	public String getDep(){

		return dep;
	}

	public void setDep(String dep){

		this.dep = dep;
	}

	public String getSex(){

		return sex;
	}

	public void setSex(String sex){

		this.sex = sex;
	}

	public String getName(){

		return name;
	}

	public void setName(String name){

		this.name = name;
	}

	public int getId(){

		return id;
	}

	public void setId(int id){

		this.id = id;
	}

	public String getBz(){

		return bz;
	}

	public void setBz(String bz){

		this.bz = bz;
	}

	@Override
	public String toString(){

		return "User{" +
				"id=" + id +
				", username='" + username + '\'' +
				", userpwd='" + userpwd + '\'' +
				", name='" + name + '\'' +
				", age=" + age +
				", sex='" + sex + '\'' +
				", phone='" + phone + '\'' +
				", qq='" + qq + '\'' +
				", t_class='" + t_class + '\'' +
				", dep='" + dep + '\'' +
				", bz='" + bz + '\'' +
				'}';
	}
}
