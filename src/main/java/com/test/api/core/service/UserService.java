package com.test.api.core.service;

import com.test.api.core.dao.*;
import com.test.api.core.model.*;
import org.springframework.stereotype.*;

import javax.annotation.*;

@Service("userService")
public class UserService{

	UserDao userDao;

	public UserDao getUserDao(){

		return userDao;
	}

	public void setUserDao(UserDao userDao){

		this.userDao = userDao;
	}

	public boolean login(User user){

		return userDao.login(user);
	}
	
}
